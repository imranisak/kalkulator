<?php
require 'FlashMessages.php'; 
if (!session_id()) @session_start();
 
$msg = new \Plasticbrain\FlashMessages\FlashMessages();
$prviBroj= mt_rand(1,10);
$drugiBroj= mt_rand(1,10);
?>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>Kalkulator hostinga</title>
  <script src="https://www.google.com/recaptcha/api.js" async defer></script>
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
  
  <link rel="stylesheet" type="text/css" href="style.css"><!--ZA LOKAL -->
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">

</head>
<body>
<div class="pt-5 col-md-8 mx-auto">
<?php $msg->display(); ?>

<form action="mail.php" method="POST" id="odabirHostinga"><!--ZA LOKAL -->
  <div id="imaLiDomenuDiv" class="odabir">
    <label>Da li imate domenu?</label><br>
    <div class="odabir form-check form-check-inline">
      <input  type="radio" name="imaDomenu" id="inlineRadio1" value="da">
      <label class="form-check-label" for="inlineRadio1">Imam domenu</label>
    </div>

    <div class="odabir form-check form-check-inline">
      <input  type="radio" name="imaDomenu" id="inlineRadio2" value="ne" checked="true">
      <label class="form-check-label" for="inlineRadio2">Nemam domenu</label>
    </div>
  </div>

<br>
<div id="odabirDomeneDiv" class="odabir">
Upišite naziv domene: 
<input class="border-0 rounded-0 form-control" type="text" name="naziv_domene" id="nazivDomene"><select id="domenaOdabir" name="domena" class="rounded-0 form-control mb-3 custom-select">
    <option value="-1"  disabled>Odaberi domenu</option>
    <option value="0" selected>.com (besplatno)</option>
    <option value="80">.ba</option>
    <option value="90">.info</option>
  </select><br>
</div>
<!--<button class="btn btn-success" id="provjeraDostupnostiDomene" disabled="false">Provjeri dostupnost domene!</button>-->
<div id="dostupnostDomene"></div>
<br>
Kreirajte hosting po Vašoj želji ili koristite već kreirane hosting pakete?<br>
<input type="radio" name="izborHostinga" value="kreiraj" id="custom" ><label class="form-check-label" for="custom">Kreiraj</label><br>
<input type="radio" name="izborHostinga" value="netreba" id="osnovni" checked="checked"><label class="form-check-label" for="osnovni">Ne treba</label><br>

<div id="hostingPaketi" class="odabir">
  <label>Odaberi hosting paket:</label><br>
  <input type="radio" name="hostingPaket" id="paket" value="50"><label class="form-check-label" for="paket">Starter</label><br>
  <input type="radio" name="hostingPaket" id="paket1" value="70"><label class="form-check-label" for="paket1">Biznis</label> <br>
  <input type="radio" name="hostingPaket" id="paket2" value="120"><label class="form-check-label" for="paket2">Profi</label><br>
  <input type="radio" name="hostingPaket" id="paket3" value="190"><label class="form-check-label" for="paket3">Premium</label><br><br>
</div>

<div id="hostingCustom" style="display: none;">

<label>Hosting po Vašem odabiru:</label><br>
<div id="odabirDiskProstoraDiv" class="odabir">
  Veličina prostora: 
  <select id="diskVelicina" name="disk" class="mb-3 custom-select-sm"><br>
      <option value="0" selected disabled>Odaberi veličinu prostora</option>
      <option value="10">2 GB disk prostora</option>
      <option value="20">3 GB disk prostora</option>
      <option value="30">4 GB disk prostora</option>
      <option value="100">Neograniceno disk prostora</option>
  </select><br>
</div>

<br>
<div id="odabirMjecesnoPrometaDiv" class="odabir"> 
Mjesecno prometa: 
  <select id="prometKolicina" name="promet" class="odabir mb-3 custom-select"><br>
      <option value="0" selected disabled>Odaberi mjesecni promet</option>
      <option value="5">10 GB prometa</option>
      <option value="10">30 GB disk prostora</option>
      <option value="30">50 GB disk prostora</option>
      <option value="100">Neograniceno prometa</option>
  </select><br>
</div>
</div>
<div id="kontaktInfo">
  Kontakt Ime:
  <input class="rounded-0 border-0 form-control" type="text" name="kontaktIme" required placeholder="Vase ime"><br>
  Kontakt Mail:
  <input class="form-control mb-3" type="mail" name="kontaktMail" required placeholder="Vas Email">
</div>
    <div class="g-recaptcha" name="g-recaptcha-response" data-sitekey="6LecaZcUAAAAAIuKNX8rT8V_l-qF3XuQm7eTWp56"></div>
<!--<div class="g-recaptcha" name="g-recaptcha-response" data-sitekey="6LecaZcUAAAAAIuKNX8rT8V_l-qF3XuQm7eTWp56"></div>-->
    <!--
Potvrdite da niste robot. Koliko je <//?php echo $prviBroj." + ".$drugiBroj."?"; ?>
<input type=number placeholder="Rezultat" name="sabiranje" class="border-0 rounded-0 form-control">
<input type=hidden name="prviBroj" value=<//?php echo $prviBroj; ?>>
<input type=hidden name="drugiBroj" value=<//?php echo $drugiBroj; ?>>
-->

<input type="text" name="cijena"  id="cijena" hidden="true">
<!--<input type="submit" value="Pošalji narudžbu" class="mt-3 btn btn-success" id="narudzbaDugme" disabled="true">-->
<button id="narudzbaDugme" form="odabirHostinga" class="mt-3 btn btn-success" disabled="true">Pošalji narudžbu <i class="fas fa-envelope"></i></button>
<button id="racunDugme" class="mt-3 btn btn-primary">IZRACUNAJ <i class="fas fa-calculator"></i></button>
</form>
<sub>Potrebno je izracunati cijenu prije slanja.</sub>
<br>
<p id="total">Ukupno: </p>
</div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js" integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js" integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k" crossorigin="anonymous"></script>
<script type="text/javascript" src="script.js"></script>
</body>
</html>