<?php

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

require 'vendor/phpmailer/phpmailer/src/Exception.php';
require 'vendor/phpmailer/phpmailer/src/PHPMailer.php';
require 'vendor/phpmailer/phpmailer/src/SMTP.php';
require 'FlashMessages.php';
//Load Composer's autoloader
require 'vendor/autoload.php';
// A session is required
if (!session_id()) @session_start();

// Instantiate the class
$msg = new \Plasticbrain\FlashMessages\FlashMessages();
$redirectTo='/';//Gdje da redirecta nakon slanja, ili nakon greske

// Display the messages
$msg->display();
$mail = new PHPMailer(true);                              // Passing `true` enables exceptions
//Recaptcha provjera (ugasena jer server ne podrzava fopen i sl.)
$recapchaResponse=$_POST['g-recaptcha-response'];
$secretKey = '6LecaZcUAAAAAKuCf1yT6YVGasW-xB4oVituMnTd';
$request = file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=".$secretKey."&response=".$recapchaResponse);
$response = json_decode($request);
$response=$response->success;
/*
//Stara provjera. Kasnije obrisati
$prviBroj=$_POST['prviBroj'];
$drugiBroj=$_POST['drugiBroj'];
$zbirTest=$prviBroj+$drugiBroj;
$unosSabiranja=$_POST['sabiranje'];
*/
if($response==true)
{
    if(!empty($_POST['imaDomenu']) && isset($_POST['imaDomenu']))
    {
            //Server settings
            $mail->Encoding = 'base64';
            $mail->CharSet = 'UTF-8';
            $mail->SMTPDebug = 0;                                 // Enable verbose debug output
            $mail->isSMTP();                                      // Set mailer to use SMTP
            $mail->Host = 'mail.webmedia.ba';  // Specify main and backup SMTP servers
            $mail->SMTPAuth = true;                               // Enable SMTP authentication
            $mail->Username = 'test@main.webmedia.ba';                 // SMTP username
            $mail->Password = 'lozinka1-1-';                           // SMTP password
            $mail->SMTPSecure = 'ssl';                            // Enable TLS encryption, `ssl` also accepted
            $mail->Port = 465;                                    // TCP port to connect to
        
            //Recipients
            $mail->setFrom('web.webmedia@gmail.com', 'Web Media Hosting');
            $mail->addAddress('web.webmedia@gmail.com');     // Add a recipient
            //Provjerava da li ima domenu
            if (isset($_POST['imaDomenu'])) 
            {
                $ima_domenu = $_POST['imaDomenu'];
                if ($ima_domenu === 'ne') 
                {
                    $ima_domenu = 'Nemam';
                }
                else if ($ima_domenu === 'da') 
                    {
                        $ima_domenu = 'Imam';
                    }
            }
            else $msg->error("Greska pri unosu. Da li imate domenu?", $redirectTo);
            /////////////////////////
            //Provjerava da li je unesen naziv domene
            if (isset($_POST['naziv_domene'])) 
            {
                $naziv_domene = $_POST['naziv_domene'];
            }
            else $msg->error("Greska pri unosu. Koji je naziv domene?", $redirectTo);
            ////////////////////////////////////
        /// Provjerava koja je domena u pitanju (.ba, .com, .net i sl.)
            if (isset($_POST['domena'])) 
            {
                $domena = $_POST['domena'];
                switch($domena)
                {
                    case '0':
                        $domena = '.com';
                    break;
                    case '80':
                        $domena = '.ba';
                    break;
                    case '90':
                        $domena = '.info';
                    break;
                }
            }
            else $msg->error("Greska pri unosu. Koja je domena u pitanju? (.ba, .com i sl.)", $redirectTo);
            ///////////////////////////////////
        /// Provjeraza koji je hosting paket izabran
            if (isset($_POST['izborHostinga'])) 
            {
                $izbor_hostinga = $_POST['izborHostinga'];
                //Ako zeli da kreira hosting po zelji, rucni odabir
                if($izbor_hostinga === 'kreiraj')
                {
                    //Provjera odabira disk prostora
                    if (isset($_POST['disk']))
                    {
                        $disk = $_POST['disk'];
                        switch($disk) 
                        {
                            case '10';
                                $disk = '2GB Disk prostora';
                            break;
                            case '20';
                                $disk = '3GB Disk prostora';
                            break;
                            case '30';
                                $disk = '4GB Disk prostora';
                            break;
                            case '100';
                                $disk = 'Neograničeno disk prostora';
                            break;
                        }
                    }
                    else $msg->error("Greska pri unosu. Koliko disk prostora zelite?", $redirectTo);
                    /////////////////////////////////
                    //Provjerava odabranu kolicinu prometa
                    if (isset($_POST['promet'])) 
                    {
                        $promet = $_POST['promet'];
                        switch($promet) 
                        {
                            case '5';
                                $promet = '10GB Bandwith';
                            break;
                            case '10';
                                $promet = '30GB Bandwith';
                            break;
                            case '30';
                                $promet = '50GB Bandwith';
                            break;
                            case '100';
                                $promet = 'Neograničeno Bandwith';
                            break;
                        }
                    }
                    else $msg->error("Greska pri unosu. Koliko prometa zelite??", $redirectTo);
                }
                ///////////////////////////////////////////
                /// Ako bira paket
                else if ($izbor_hostinga === 'netreba')
                {
                    if (isset($_POST['hostingPaket']))
                    {
                        $hosting_paket = $_POST['hostingPaket'];
                        switch($hosting_paket)
                        {
                            case '50':
                                $hosting_paket = 'STARTER';
                            break;
                            case '70':
                                $hosting_paket = 'BIZNIS';
                            break;
                            case '120':
                                $hosting_paket = 'PROFI';
                            break;
                            case '190':
                                $hosting_paket = 'PREMIUM';
                            break;
                        }
                    }
                    else $msg->error("Greska pri unosu. Odaberite paket", $redirectTo);
                }
                //////////////////////
                /// Kontakt ime
                if (isset($_POST['kontaktIme'])) 
                {
                    $ime_kontakt = $_POST['kontaktIme'];
                }
                else $msg->error("Greska pri unosu. Unesite ime.", $redirectTo);
                /////////////////////
                /// Kontakt mail
                if (isset($_POST['kontaktMail'])) 
                {
                    $mail_kontakt = $_POST['kontaktMail'];
                }
                else $msg->error("Greska pri unosu. Unesite kontakt mail", $redirectTo);
                //////////////////
                /// Cijena
                if (isset($_POST['cijena'])) 
                {
                    $cijena = $_POST['cijena'];
                }
                else $msg->error("Greska pri racunanju cijene. Molimo pokusajte ponovo.", $redirectTo);
            }

            $replyto = $_POST['kontaktMail'];
        
            //Content
            $webMediaLogo="<div style='background-color:#2593e5;display:block;text-align:center;margin:0 auto 30px;'><a href='https://webmedia.ba' target='_blank'><img src='https://webmedia.ba/wp-content/uploads/2019/02/logotip-webmedia-1.png'></a></div>";
            $footer ="<div style='background-color:#222;display:block;margin:30px auto 0;text-align:center;padding:20px;'><a href='https://www.facebook.com/webmediasarajevo/' style='margin:0 2px;' target='_blank'><img src='https://iconsplace.com/wp-content/uploads/_icons/ffffff/256/png/facebook-icon-18-256.png' width='25' height='25'></a><a href='https://www.instagram.com/webmediasarajevo/' target='_blank'><img src='https://toppng.com/public/uploads/preview/instagram-logo-white-on-black-11549378348xpa0ozbbcp.png' style='margin:0 2px;' width='25' height='25'></a><br><a href='https://webmedia.ba' target='_blank' style='color:#fff;text-decoration:none;'>Web Media d.o.o. Sarajevo</a></div>";
            $mail->isHTML(true);                                  // Set email format to HTML
            $mail->Subject = 'Hosting Narudžba';
            if ($izbor_hostinga === 'netreba') {
            $mail->Body   = "$webMediaLogo Ime: $ime_kontakt<br>
            Email: $mail_kontakt<br>Da li imate domenu: $ima_domenu <br> 
            Domena: $naziv_domene$domena<br>
            Hosting paket: $hosting_paket<br>
            Cijena: $cijena KM<br>$footer";
            }
            else  {
            $mail->Body    = "$webMediaLogo Ime: $ime_kontakt<br>
            Email: $mail_kontakt<br>Da li imate domenu: $ima_domenu <br> 
            Domena: $naziv_domene$domena<br>
            Custom paket: Disk prostor: $disk<br> Promet: $promet<br>
            Cijena: $cijena KM<br>$footer";
            }
            $mail->AltBody = 'This is the body in plain text for non-HTML mail clients';
            
            //ZA KLIJENTA//////////////////////////////////////////////////////////////////////////////
            $mail->send();
            $mail->ClearAllRecipients();
            // alternative in this case (only addresses, no cc, bcc): 
            // $mail->ClearAddresses();
            $mail->Subject = 'Hosting Narudžba primljena';
            if ($izbor_hostinga === 'netreba') {
            $mail->Body    = "$webMediaLogo Poštovani $ime_kontakt,<br><br>Vaša narudžba je primljena:<br><b>Da li imate domenu:</b> $ima_domenu <br> 
            <b>Domena:</b> $naziv_domene$domena<br>
            Hosting paket: $hosting_paket<br>
            <b>Cijena:</b> $cijena KM<br>$footer";
            }
            else  {
            $mail->Body    = "$webMediaLogo Poštovani $ime_kontakt,<br><br>Vaša narudžba je primljena:<br><b>Da li imate domenu:</b> $ima_domenu <br> 
            <b>Domena:</b> $naziv_domene$domena<br>
            <b>Custom paket:</b> Disk prostor: $disk<br> Promet: $promet<br>
            <b>Cijena:</b> $cijena KM<br>$footer";
            }
            //$adminemail = $generalsettings[0]["admin_email"]; 

            // Add the admin address
            $mail->AddAddress($replyto);
            $mail->Send();
            $success = 'Message has been sent';
            $msg->success('Narudžba poslana! Ako ne vidite mail u Vašem inboxu, pogledajte SPAM folder.', $redirectTo);
    }
}
//Ako reCaptcha ne vazi
else $msg->error('Nevazeca reCaptcha! Molimo Vas da pokušate ponovo!', $redirectTo);

?>

<!doctype html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>Kalkulator hostinga</title>

  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
  <link rel="stylesheet" type="text/css" href="style.css">

</head>
<body>
</body>
</html>