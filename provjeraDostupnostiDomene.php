<?php
    $nazivDomene=$_GET['nazivDomene'];
    $domena=$_GET['domena'];
    $domena=strtok($domena," ");
    $domenaZaProvjeru=$nazivDomene.$domena;
    //echo $domenaZaProvjeru;
    // set error message by default
    //$msg = '<h3 class="text-center alert alert-danger">Upišite validnu domenu</h3>';

    // string replace or clean $domenaZaProvjeru to ensure its nothing but domain and tld
    $domenaZaProvjeru = str_replace('www.', '', $domenaZaProvjeru);
    $domenaZaProvjeru = str_replace('www', '', $domenaZaProvjeru);
    $domenaZaProvjeru = str_replace('/', '', $domenaZaProvjeru);
    $domenaZaProvjeru = str_replace(':', '', $domenaZaProvjeru);
    $domenaZaProvjeru = str_replace('https', '', $domenaZaProvjeru);
    $domenaZaProvjeru = str_replace('http', '', $domenaZaProvjeru);
    $domenaZaProvjeru = trim($domenaZaProvjeru);
    $domenaZaProvjeru = filter_var($domenaZaProvjeru, FILTER_SANITIZE_URL);
    

    // if $domenaZaProvjeru is NOT empty, means contains a value
    if($domenaZaProvjeru){

        // see GoDaddy API documentation - https://developer.godaddy.com/doc
        // url to check domain availability
        $url = "https://api.godaddy.com/v1/domains/available?domain=".$domenaZaProvjeru;

        // see GoDaddy API documentation - https://developer.godaddy.com/doc
        // set your key and secret
        $header = array(
            'Authorization: sso-key 9ER591udY9i_URhLtX6pKeZKWj2B6PCitW:URhShrQjc8C2UmQXx3Z8rJ'
        );

        //open connection
        $ch = curl_init();
        $timeout=60;

        //set the url and other options for curl
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER,false);  
        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);

        //execute post and return response data.
        $result = curl_exec($ch);

        //close curl connection
        curl_close($ch);

        // decode the json response
        $dn = json_decode($result, true);

        // check if error code
        if(isset($dn['code'])){

            /*$msg = explode(":",$dn['message']);

            $msg = '<h3 style="text-align:center;" class="alert alert-danger">Domena nije pravilno unesena</h3>';*/
            echo '-1';//Nepravilan unos

        // proceed if no error
        } else {

            // the domain is available
            if(isset($dn['available']) && $dn['available'] == true){

               /* $msg = '<h3 style="text-align:center;" class="alert alert-success">'.$domenaZaProvjeru.' je <span style="color:green">dostupna!</span></h3>';*/
               echo '1';//Dostupna domena

            // else the domain is NOT available
            } else {

                /*$msg = '<h3 style="text-align:center;" class="alert alert-danger"> '.$domenaZaProvjeru.' je <span style="color:red">zauzeta!</span></h3>';*/
                echo '0';//Nedostupna domena

            }
        
        }

    }


?>