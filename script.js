$(document).ready(function()
{
	var customHosting=false,
		zauzetaDomena=true,
		nemaDomenu=true;
	$('input[type=radio][name=izborHostinga]').change(function() {
	    if (this.value=="netreba") 
	    {
	        $("#hostingPaketi").show();
	        $("#hostingCustom").hide();
	        customHosting=false;
	    }
	    else if (this.value=="kreiraj") 
	    {
	        $("#hostingPaketi").hide();
	        $("#hostingCustom").show();
	        customHosting=true;
	    }
	});	

	$('input[type=radio][name=imaDomenu]').change(function() 
	{
		if(this.value=='ne')
		{
			nemaDomenu=true;
			zauzetaDomena=false;
			provjeraDomene();
		}
		else
		{
			nemaDomenu=false;
			zauzetaDomena=false;
			$("#dostupnostDomene").text("");
		}
	});

	function odabirGreska(greska, div) 
	{
		$(div).css({
					/*"border-style":"solid",
					"border-width": "5px",
					"border-color": "red",*/
					"backgroundColor" : "rgba(250,0,0,0.3)",
					});
	}
	function ugasiDugmeZaSlanje()
	{
		$('#narudzbaDugme').prop('disabled', true);
	}

	function upaliDugmeZaSlanje()
	{
		$('#narudzbaDugme').prop('disabled', false);
	}
	function ugasiGresku() 
	{
		$(".odabir").css({"backgroundColor" : "rgba(250,0,0,0.0)"});
	}

	function provjeraDomene()
	{
	if(nemaDomenu)
		{	
		setTimeout(1000);
				$.ajax({
					url:'/provjeraDostupnostiDomene.php',
					type:'get',
					data:{nazivDomene: $("#nazivDomene").val(), 
					domena: $('#domenaOdabir').find(":selected").text()},
					success: function(data)
					{
						if(data==1)
						{
							zauzetaDomena=false;
							$("#dostupnostDomene").text("Domena je dostupna!");
							$("#dostupnostDomene").css({
							"backgroundColor" : "rgba(0,250,0,0.3)"
							})
						}				
						else if(data==0)
						{
							zauzetaDomena=true;
							$("#dostupnostDomene").text("Domena je nedostupna!");
							$("#dostupnostDomene").css({
							"backgroundColor" : "rgba(250,0,0,0.3)"
							})
						}				

						else if(data==-1)
						{
							ugasiDugmeZaSlanje();
							$("#dostupnostDomene").text("Greska pri unosu!");
							$("#dostupnostDomene").css({
							"backgroundColor" : "rgba(250,0,0,0.3)"
							})
						}
					},
					error: function ()
					{
						$("#dostupnostDomene").text("Doslo je do greske. Pokusajte ponovo kasnije!");
						$("#dostupnostDomene").css({
						"backgroundColor" : "rgba(250,0,0,0.3)"
						})
					}

				});
			}
	}
$("#odabirDomeneDiv").change(function()
{
	provjeraDomene();
})

	//Racuna cijenu
	$("#racunDugme").click(function(e)
	{
		e.preventDefault();
		var total=0;
		if (customHosting)
		{
			ugasiGresku();
			var cijenaVelicinaDiska=parseInt($('#diskVelicina').find(":selected").val()),
				cijenaMjesecnoPrometa=parseInt($('#prometKolicina').find(":selected").val()),
				cijenaDomene=parseInt($('#domenaOdabir').find(":selected").val()),
				imaDomenu=$('input[name=imaDomenu]:checked').val(),
				cijena=0, //Cijena ide u mail kao broj, dok total sluzi samo za ispis u HTMLu
				nazivDomene=$("#nazivDomene").val();
			if (!imaDomenu) 
			{
				odabirGreska(true, "#imaLiDomenuDiv");
				ugasiDugmeZaSlanje();
				return 0;
			}
			if (cijenaDomene==-1 || !nazivDomene)
			{
				odabirGreska(true, "#odabirDomeneDiv");
				ugasiDugmeZaSlanje();
				return 0;
			}

			if (cijenaVelicinaDiska==0) 
			{
				ugasiDugmeZaSlanje();
				odabirGreska(true,"#odabirDiskProstoraDiv");
				return 0;
			}
			if (cijenaMjesecnoPrometa==0)
			{
				ugasiDugmeZaSlanje();
				odabirGreska(true, "#odabirMjecesnoPrometaDiv");
				return 0;
			}
			total=cijenaVelicinaDiska+cijenaMjesecnoPrometa;
			if(imaDomenu=="ne") total+=cijenaDomene;
			cijena=total;//Cijena ide u mail kao broj, dok total sluzi samo za ispis u HTMLu
			total="Total: "+total+" KM";
			if(!zauzetaDomena) upaliDugmeZaSlanje();
			$("#cijena").val(cijena);
			$("#total").text(total);
		}
		else 
		{
			var cijenaPaketa=parseInt($('input[name=hostingPaket]:checked').val()),
				cijenaDomene=parseInt($('#domenaOdabir').find(":selected").val()),
				imaDomenu=$('input[name=imaDomenu]:checked').val(),
				cijena=0,
				nazivDomene=$("#nazivDomene").val();
			ugasiGresku();
			if (!imaDomenu) 
			{
				odabirGreska(true, "#imaLiDomenuDiv");
				ugasiDugmeZaSlanje();
				return 0;
			}
			if (cijenaDomene==-1 || !nazivDomene)
			{
				odabirGreska(true, "#odabirDomeneDiv");
				ugasiDugmeZaSlanje();
				return 0;
			}
			if(!cijenaPaketa)
			{
				odabirGreska(true, "#hostingPaketi");
				ugasiDugmeZaSlanje();
				return 0;
			}
			total+=cijenaPaketa;
			if(imaDomenu=="ne") total+=cijenaDomene;
			cijena=total;//Cijena ide u mail kao broj, dok total sluzi samo za ispis u HTMLu
			total="Total: "+total+" KM";
			if(!zauzetaDomena) upaliDugmeZaSlanje();
			$("#cijena").val(cijena);
			$("#total").text(total);
			console.log(imaDomenu);
		}
	});
	$(document).change(function()
	{
		ugasiDugmeZaSlanje();
	})
});
